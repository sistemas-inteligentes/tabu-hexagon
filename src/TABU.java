import java.util.ArrayList;

public class TABU {
	//FALTA ACTUALIZAR PERO NO CREO QUE SEA NECESARIO
	ArrayList<Hexagon> Lista_Tabu = new ArrayList<Hexagon>();	
	
	//ES UN AUXILIAR EN POCAS PALABRAS
	ArrayList<Hexagon> EspaciosDeEstadosAdyacentes = new ArrayList<Hexagon>();	
	
	//REGLAS APLICABLES
	ArrayList<Hexagon> ReglasAplicables = new ArrayList<Hexagon>();	
	
	Hexagon Est_Nuevo = new Hexagon();
	Hexagon Est_Actual = new Hexagon();
	Hexagon Est_mejor_por_ahora = new Hexagon();
	Hexagon Est_candidato = new Hexagon();
	
	
	int valor,valor_ant,j,Num_Iter,Tam = 0;
	int Num=1;
	boolean exito;
	public long inicio = 0 ;
    public long fin = 0;
	public int EE_Repetidos=0;
	
	
	TABU(int num_iter){
		this.Num_Iter=num_iter;
	}
	
	public void Mostrar(Hexagon Estado_Inicial) {
		Estado_Inicial.Mostrar();
	}
	
	private boolean Condicion_term (Hexagon c)
	{
		if (c.Calcular_h() >= 12)
		{
			return true;
		}
		return false;
	}
	
	public boolean TABU_BETA() {
		// COMENZANDO CONSTRUCCION DEL TABU
		Est_Actual.LeerDatos();
		inicio = System.currentTimeMillis();
		System.out.println("ENTRADA");
		Est_Actual.Mostrar();
		Insertar(Est_Actual);
		Copiar_Estado(Est_mejor_por_ahora,Est_Actual);
		valor_ant = Est_mejor_por_ahora.Calcular_h();
		exito = false;
		j=1;
		while (!Condicion_term(Est_Actual) && j<Num_Iter) 
		{
			Tam = ReglasAplicables(Est_Actual);
			for (int i = 0; i < Tam; i++) 
			{
				Copiar_Estado(Est_Nuevo,ReglasAplicables.get(i));
				valor = Est_Nuevo.Calcular_h();
				if (!NoBuscar(Est_Nuevo)) 
				{
					if (valor>=Est_mejor_por_ahora.Calcular_h())
					{
						Copiar_Estado(Est_mejor_por_ahora,Est_Nuevo);
						Num=j;
					}
					else
					{
						if (valor>valor_ant || Tam == 1)
						{
							Copiar_Estado(Est_candidato, Est_Nuevo);
							Num=0;
						}
					}
				}
				else {
					EE_Repetidos++;
				}
				
			}
			if (Num==0)
			{
				Copiar_Estado(Est_mejor_por_ahora, Est_candidato);
			}
			if (Tam==0)
			{
				Est_Actual.SacarYColocarHexagono(0, j%7);
				Est_Actual.Iniciar();
				Est_Actual.PiezasColocadas=1;
			}
			else {
				Actualizar(j);			
				Insertar(Est_mejor_por_ahora);
				Copiar_Estado(Est_Actual, Est_mejor_por_ahora);
			}			
			/*System.out.println("TABU");
			MostrarTABU();	
			Est_Actual.Mostrar();
			valor = Est_Actual.Calcular_h();
			System.out.println("H :: " + valor);*/
			j++;
		}
		if (Condicion_term(Est_Actual))
		{
			exito = true;
		}
		

		MostrarDatosDeEvaluacion();
	    return exito;
	}

	private void MostrarDatosDeEvaluacion() {
		fin = System.currentTimeMillis();
		long tiempo = (long) ((fin - inicio));
	    System.out.println((tiempo) +" milisegundos");
		System.out.println("VECES QUE SE ENCONTRARON E.E. REPETIDOS  :: " +  EE_Repetidos);
	}


	private void Actualizar(int j)
	{
		if (j%5== 0)
		{
			Lista_Tabu.remove(0);
		}
		ReglasAplicables.clear();
	}

	private void MostrarReglas() {
		for (int i = 0; i < ReglasAplicables.size(); i++) {
			System.out.println("HEXAGONO :: " + (i+1));
			System.out.println("PIEZAS :: " + ReglasAplicables.get(i).PiezasColocadas);
			System.out.println("VALOR H :: " + ReglasAplicables.get(i).Calcular_h());
		
			ReglasAplicables.get(i).Mostrar();
			}
	}
	
	private void Copiar_Estado(Hexagon Est_mejor,Hexagon Est_Actual) {
		Est_mejor.CopiarMatriz(Est_Actual.ObtenerMatriz());
		Est_mejor.PiezasColocadas=Est_Actual.PiezasColocadas;
	}


	private int ReglasAplicables(Hexagon Est_Actual) {
		int Val=0;		
		for (int j = Est_Actual.PiezasColocadas; j < 7; j++) {
			Hexagon Aux = new Hexagon();
			EspaciosDeEstadosAdyacentes.add(Aux);
			Copiar_Estado(EspaciosDeEstadosAdyacentes.get(Val), Est_Actual);
			if(EspaciosDeEstadosAdyacentes.get(Val).AplicarRegla(EspaciosDeEstadosAdyacentes.get(Val).PiezasColocadas, j))
			{
				Aux.CopiarMatriz(EspaciosDeEstadosAdyacentes.get(Val).ObtenerMatriz());
				Aux.PiezasColocadas=EspaciosDeEstadosAdyacentes.get(Val).PiezasColocadas;
				ReglasAplicables.add(Aux);
			}
			Val++;
		}
		return ReglasAplicables.size();
	}
	
	
	// SOLO PARA VERIFICAR SI INSERTA BIEN
	
	public void Insertar(Hexagon Estado_Visitado) {
		Hexagon Aux = new Hexagon();
		Aux.CopiarMatriz(Estado_Visitado.ObtenerMatriz());
		if (!NoBuscar(Aux)) {
			Lista_Tabu.add(Aux);	
		}
	}
	// SOLO PRUEBAS
	
	public void MostrarTABU() {
		for (int i = 0; i <Lista_Tabu.size(); i++) {
			Lista_Tabu.get(i).Mostrar();
		}
	}
	
	//FUNCIONA EL BUSCAR TABU V,:
	
	public boolean NoBuscar(Hexagon Estado_Visitado) {
		for (int i = 0; i < Lista_Tabu.size(); i++) {
		
			if (Lista_Tabu.get(i).SonIguales(Estado_Visitado))
				return true;
		}
		return false;
	}

	public void MostrarSolucion() {
		Est_Actual.Mostrar();
		System.out.println("HEURISTICA :: " + Est_Actual.Calcular_h());
		System.out.println("NUMERO DE PIEZAS COLOCADAS :: " + Est_Actual.PiezasColocadas);
		System.out.println("NODOS :: " + j);

	}
}
